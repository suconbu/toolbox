﻿using System.IO;
using System.Text;
using System.Threading;
using System.Diagnostics;
using System.Windows.Forms;
using System.Collections.Concurrent;

namespace Suconbu.Toolbox
{
    public enum LogLevel
    {
        Error,
        Warn,
        Info,
        Time,
        Count,
        Marker,
    }

    /// <summary>
    /// ログ出力用の静的メソッドを提供します。
    /// ログ表示用のフォームとして Form プロパティを提供します。
    /// </summary>
    public class Log
    {
        /// <summary>
        /// ログを表示するフォームを取得します。
        /// </summary>
        public static Form Form { get { return instance.form; } }

        static Log instance = new Log();

        LogForm form = new LogForm();
        Stopwatch stopwatch = Stopwatch.StartNew();
        ConcurrentDictionary<string, Stopwatch> timers = new ConcurrentDictionary<string, Stopwatch>();
        ConcurrentDictionary<string, int> counters = new ConcurrentDictionary<string, int>();
        ConcurrentDictionary<int, int> indentLevels = new ConcurrentDictionary<int, int>();

        /// <summary>
        /// ログをInfoレベルで出力します。
        /// </summary>
        public static void Info(string format, params object[] args)
        {
            instance.PushLog(LogLevel.Info, string.Format(format, args));
        }

        /// <summary>
        /// ログをWarnレベルで出力します。
        /// </summary>
        public static void Warn(string format, params object[] args)
        {
            instance.PushLog(LogLevel.Warn, string.Format(format, args));
        }

        /// <summary>
        /// ログをErrorレベルで出力します。
        /// </summary>
        public static void Error(string format, params object[] args)
        {
            instance.PushLog(LogLevel.Error, string.Format(format, args));
        }

        /// <summary>
        /// 条件が不成立の場合に任意の文字列をErrorレベルで出力します。
        /// 文字列を省略した場合は呼び出し元の関数名を使います。
        /// </summary>
        public static void Assert(bool assertion, string format = null, params object[] args)
        {
            if (!assertion)
            {
                var t = (format != null) ? string.Format(format, args) : instance.GetCallerLocationText();
                instance.PushLog(LogLevel.Error, "Assertion failed : " + t);
            }
        }

        /// <summary>
        /// 時間計測を開始します。
        /// name を省略した場合は呼び出し元の関数名を使います。
        /// </summary>
        public static void Time(string name = null)
        {
            name = name ?? instance.GetCallerMethodName();
            instance.timers[name] = Stopwatch.StartNew();
        }

        /// <summary>
        /// 同じ name で開始された時間計測を終了して結果を出力します。
        /// name を省略した場合は呼び出し元の関数名を使います。
        /// </summary>
        public static long TimeEnd(string name = null)
        {
            name = name ?? instance.GetCallerMethodName();
            if (instance.timers.TryRemove(name, out var timer))
            {
                timer.Stop();
                var label = name ?? instance.GetCallerLocationText();
                instance.PushLog(LogLevel.Time, string.Format("{0} : {1} msec", label, timer.ElapsedMilliseconds));
                return timer.ElapsedMilliseconds;
            }
            return 0;
        }

        /// <summary>
        /// 回数をInfoレベルで出力します。
        /// name を省略した場合は呼び出し箇所のファイル名と行番号を使います。
        /// </summary>
        public static int Count(string name = null)
        {
            name = name ?? instance.GetCallerLocationText();
            if (instance.counters.TryGetValue(name, out var count))
            {
                instance.counters[name] = ++count;
            }
            else
            {
                count = 1;
                instance.counters[name] = count;
            }
            instance.PushLog(LogLevel.Count, string.Format("{0} : {1} ", name, count));
            return count;
        }

        /// <summary>
        /// 以降のログを GroupEnd を呼び出すまで一段階インデントします。
        /// </summary>
        public static void Group(string label = "")
        {
            instance.PushLog(LogLevel.Info, label + " {");
            int cid = Thread.CurrentContext.ContextID;
            if (instance.indentLevels.TryGetValue(cid, out int level))
            {
                instance.indentLevels[cid] = level + 1;
            }
            else
            {
                instance.indentLevels[cid] = 1;
            }
        }

        /// <summary>
        /// インデントを一段階戻します。
        /// インデントされていない状態で呼び出した場合は何もしません。
        /// </summary>
        public static void GroupEnd()
        {
            int cid = Thread.CurrentContext.ContextID;
            if (instance.indentLevels.TryGetValue(cid, out int level))
            {
                if (level > 0)
                {
                    instance.indentLevels[cid] = level - 1;
                }
            }
            instance.PushLog(LogLevel.Info, "}");
        }

        void PushLog(LogLevel level, string log)
        {
            int indentLevel = instance.GetCurrentIndentLevel();
            var spaces = (new StringBuilder()).Insert(0, "    ", indentLevel).ToString();
            var frames = new StackTrace(2, true).GetFrames();
            instance.form.PushLog(instance.stopwatch.ElapsedMilliseconds / 1000.0, level, spaces + log, frames);
        }

        int GetCurrentIndentLevel()
        {
            int cid = Thread.CurrentContext.ContextID;
            int level = 0;
            instance.indentLevels.TryGetValue(cid, out level);
            return level;
        }

        string GetCallerMethodName()
        {
            return new StackTrace(true).GetFrame(2).GetMethod().DeclaringType.Name;
        }

        string GetCallerLocationText()
        {
            var frame = new StackTrace(true).GetFrame(2);
            var fileName = Path.GetFileName(frame.GetFileName());
            int lineNo = frame.GetFileLineNumber();
            return string.Format("{0}:{1}", fileName, lineNo);
        }

        Log()
        {
            this.form.MarkerButtonClicked += (s, markerNo) =>
            {
                instance.PushLog(LogLevel.Marker, "Marker : " + markerNo.ToString());
            };
        }
    }
}
