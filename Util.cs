﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Windows.Forms;
using System.Drawing;
using System.Reflection;
using System.Diagnostics;

namespace Suconbu.Toolbox
{
    public static class Util
    {
        /// <summary>
        /// 指定したコントロールとその子孫コントロールに対してコールバックを実行します。
        /// </summary>
        public static void TraverseControls(Control control, Action<Control> action)
        {
            foreach (Control c in control.Controls)
            {
                TraverseControls(c, action);
            }
            action(control);
        }

        /// <summary>
        /// バージョン文字列を取得します。
        /// 例：1.2.3.4
        /// </summary>
        public static string GetVersionString(int fieldCount = 4)
        {
            return Assembly.GetExecutingAssembly().GetName().Version.ToString(fieldCount);
        }

        /// <summary>
        /// 自身のアプリケーション名を取得します。
        /// 例：toolboxTester
        /// </summary>
        public static string GetApplicationName()
        {
            return Assembly.GetExecutingAssembly().GetName().Name;
        }

        /// <summary>
        /// コピーライト文字列を取得します。
        /// 例：Copyright © 2018 D.Miwa
        /// </summary>
        public static string GetCopyrightString()
        {
            // 参考：http://dobon.net/vb/dotnet/sectionFile/myversioninfo.html
            return ((AssemblyCopyrightAttribute)Attribute.GetCustomAttribute(
                Assembly.GetExecutingAssembly(),
                typeof(AssemblyCopyrightAttribute))).Copyright;
        }

        /// <summary>
        /// 現在の行を含むメソッドの名前空間付きの名前を取得します。
        /// </summary>
        public static string GetCurrentMethodName(bool fullName = false)
        {
            return fullName ?
                new StackTrace().GetFrame(1).GetMethod().DeclaringType.FullName :
                new StackTrace().GetFrame(1).GetMethod().DeclaringType.Name;
        }

        public static Control GetSourceControl(ToolStripItem item)
        {
            if (item == null) return null;
            var menu = item.GetCurrentParent() as ContextMenuStrip;
            if (menu == null) return null;
            return menu.SourceControl;
        }

        public static string GetName<T>(Expression<Func<T>> e)
        {
            return ((MemberExpression)e.Body).Member.Name;
        }

        public static Image TakeScreenshot(Control control)
        {
            return TakeScreenshotInside(new Rectangle(control.PointToScreen(control.Location), control.Size));
        }

        public static Image TakeScreenshot(Rectangle bounds)
        {
            return TakeScreenshotInside(bounds);
        }

        static Image TakeScreenshotInside(Rectangle bounds)
        {
            var bitmap = new Bitmap(bounds.Width, bounds.Height);
            Graphics.FromImage(bitmap).CopyFromScreen(bounds.Location, new Point(), bounds.Size);
            return bitmap;
        }
    }
}
