﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Suconbu.Toolbox;
using System.Windows.Forms.DataVisualization.Charting;
using System.IO;
using System.Diagnostics;

namespace Suconbu.Toolbox
{
    /// <summary>
    /// ログを表示します。
    /// </summary>
    public partial class LogForm : Form
    {
        public event EventHandler<int> MarkerButtonClicked = delegate { };

        FlickerFreeListView listView = new FlickerFreeListView();
        Chart chartView = new Chart();
        Dictionary<int, ListViewItem> itemCache = new Dictionary<int, ListViewItem>();
        SplitContainer mainSplit = new SplitContainer();
        Dictionary<LogLevel, int> countByLogLevel = new Dictionary<LogLevel, int>();
        Dictionary<LogLevel, ToolStripButton> filterButtons = new Dictionary<LogLevel, ToolStripButton>();
        ToolStripButton lockButton = new ToolStripButton();
        ToolStripTextBox filterTextBox = new ToolStripTextBox();
        ToolStripLabel totalMemoryLabel = new ToolStripLabel();
        ToolStripLabel elapseLabel = new ToolStripLabel();
        ToolStripButton saveButton = new ToolStripButton();
        ToolStripStatusLabel callerLabel = new ToolStripStatusLabel();
        Stopwatch elapsedTimer = Stopwatch.StartNew();
        List<LogRecord> temporaryLogRecords = new List<LogRecord>();
        List<LogRecord> logRecords = new List<LogRecord>();
        IEnumerable<LogRecord> filteredLogRecords;
        long[] totalMemorySizes = new long[600];
        int totalMemorySizeStartIndex = 0;
        int totalMemorySizeCount = 0;
        bool scrollLocked = false;
        int nextMarkerNo = 1;

        readonly Size defaultFormSize = new Size(800, 300);
        readonly int timeColumnWidth = 100;
        readonly int fromColumnWidth = 240;
        readonly int maxItemCacheCount = 64;
        readonly int maxListViewLine = 64 * 1024;
        readonly Dictionary<LogLevel, string> logLevelToImageKey = new Dictionary<LogLevel, string>()
        {
            { LogLevel.Error, "exclamation.png" },
            { LogLevel.Warn, "error.png" },
            { LogLevel.Info, "information.png" },
            { LogLevel.Time, "time.png" },
            { LogLevel.Count, "arrow_rotate_clockwise.png" },
            { LogLevel.Marker, "flag_red.png" },
        };

        /// <summary>
        /// インスタンスを作成します。
        /// </summary>
        public LogForm()
        {
            InitializeComponent();

            this.Text = "Log monitor";

            foreach (var level in (IEnumerable<LogLevel>)Enum.GetValues(typeof(LogLevel)))
            {
                this.countByLogLevel[level] = 0;
            }

            var statusUpdateTimer = new Timer();
            statusUpdateTimer.Interval = 1000;
            statusUpdateTimer.Tick += (s, e) =>
            {
                if (this.Visible)
                {
                    int index = (this.totalMemorySizeStartIndex + this.totalMemorySizeCount) % this.totalMemorySizes.Length;
                    long size = GC.GetTotalMemory(true);
                    this.totalMemorySizes[index] = size;
                    this.totalMemoryLabel.Text = string.Format("{0:0.000} MB", size / 1024.0 / 1024.0);
                    if (this.totalMemorySizeCount < this.totalMemorySizes.Length)
                    {
                        this.totalMemorySizeCount++;
                    }
                    else
                    {
                        if (++this.totalMemorySizeStartIndex >= this.totalMemorySizes.Length)
                        {
                            this.totalMemorySizeStartIndex = 0;
                        }
                    }

                    this.elapseLabel.Text = string.Format("{0:0} sec", this.elapsedTimer.Elapsed.TotalSeconds);
                }
            };

            this.Load += (s, e) =>
            {
                this.SetupControls();
                statusUpdateTimer.Start();
            };

            this.FormClosing += (s, e) =>
            {
                e.Cancel = true;
                this.Hide();
            };
        }

        /// <summary>
        /// ログを追加します。
        /// 複数のスレッドからの呼び出しを許容します。
        /// </summary>
        public void PushLog(double elapsedSeconds, LogLevel level, string log, StackFrame[] frames)
        {
            lock (this)
            {
                this.temporaryLogRecords.Add(
                    new LogRecord(this.logRecords.Count, elapsedSeconds, level, log, frames[0]));
                this.countByLogLevel[level]++;
            }

            if (this.Visible)
            {
                Delay.SetTimeout(() =>
                {
                    this.saveButton.Enabled = true;
                    lock (this)
                    {
                        this.logRecords.AddRange(this.temporaryLogRecords);
                        this.temporaryLogRecords.Clear();
                    }
                    this.UpdateListContent();
                    this.UpdateFilterButtonLabel();
                }, 100, this, Util.GetCurrentMethodName(true));
            }
        }

        void SetupControls()
        {
            this.Size = this.defaultFormSize;

            this.SetupToolStrip();
            this.SetupStatusStrip();
            this.SetupListView();

            this.toolStripContainer1.ContentPanel.Controls.Add(this.listView);

            Util.TraverseControls(this, c => c.Font = SystemFonts.MessageBoxFont);

            this.UpdateFilterButtonLabel();
        }

        void SetupToolStrip()
        {
            this.toolStrip1.GripStyle = ToolStripGripStyle.Hidden;

            foreach (var level in (IEnumerable<LogLevel>)Enum.GetValues(typeof(LogLevel)))
            {
                var button = new ToolStripButton(this.imageList1.Images[this.logLevelToImageKey[level]]);
                button.CheckOnClick = true;
                button.DoubleClickEnabled = true;
                button.Checked = true;
                button.CheckedChanged += (s, e) =>
                {
                    Delay.SetTimeout(() => this.UpdateListContent(), 300, this, Util.GetCurrentMethodName(true));
                };
                button.DoubleClick += (s, e) =>
                {
                    if (this.filterButtons.Values.All(b => !b.Checked))
                    {
                        // 全部未チェックだったら全選択
                        foreach (var b in this.filterButtons.Values)
                        {
                            b.Checked = true;
                        }
                    }
                    else
                    {
                        foreach (var b in this.filterButtons.Values)
                        {
                            b.Checked = b == s;
                        }
                    }
                };
                this.filterButtons.Add(level, button);
            }
            this.toolStrip1.Items.AddRange(this.filterButtons.Values.ToArray());

            this.toolStrip1.Items.Add(new ToolStripSeparator());

            var markerButton = new ToolStripButton("Set marker 1", this.imageList1.Images["flag_red.png"]);
            markerButton.Click += (s, e) =>
            {
                this.MarkerButtonClicked(this, this.nextMarkerNo++);
                markerButton.Text = "Set marker " + this.nextMarkerNo;
            };
            this.toolStrip1.Items.Add(markerButton);

            this.toolStrip1.Items.Add(new ToolStripSeparator());

            this.saveButton.Image = this.imageList1.Images["disk.png"];
            this.saveButton.Text = "Save";
            this.saveButton.Click += (s, e) =>
            {
                this.SaveLogInfos(Application.StartupPath);
            };
            this.toolStrip1.Items.Add(this.saveButton);
        }

        void SetupStatusStrip()
        {
            var clearTextButton = new ToolStripButton(this.imageList1.Images["cross.png"]);
            clearTextButton.Visible = false;
            clearTextButton.Click += (s, e) => this.filterTextBox.Clear();

            this.filterTextBox.TextChanged += (s, e) =>
            {
                clearTextButton.Visible = (this.filterTextBox.Text.Length > 0);
                this.filterTextBox.Focus();
                Delay.SetTimeout(() => this.UpdateListContent(), 100, this, Util.GetCurrentMethodName(true));
            };

            this.statusStrip1.Items.Add(new ToolStripStatusLabel("Filter"));
            this.statusStrip1.Items.Add(this.filterTextBox);
            this.statusStrip1.Items.Add(clearTextButton);

            this.statusStrip1.Items.Add(new ToolStripSeparator());

            this.callerLabel.TextAlign = ContentAlignment.MiddleLeft;
            this.callerLabel.Spring = true;
            this.statusStrip1.Items.Add(this.callerLabel);

            this.lockButton.Image = this.imageList1.Images["arrow_down.png"];
            this.lockButton.Text = "AutoScroll";
            this.lockButton.Click += (s, e) => this.SetScrollLock(!this.scrollLocked);
            this.statusStrip1.Items.Add(this.lockButton);

            this.statusStrip1.Items.Add(new ToolStripSeparator());

            this.totalMemoryLabel = new ToolStripLabel(this.imageList1.Images["brick.png"]);
            this.totalMemoryLabel.Alignment = ToolStripItemAlignment.Right;
            this.totalMemoryLabel.ToolTipText = "GC.GetTotalMemory(true)";
            this.statusStrip1.Items.Add(this.totalMemoryLabel);

            this.statusStrip1.Items.Add(new ToolStripSeparator());

            this.elapseLabel = new ToolStripLabel(this.imageList1.Images["clock.png"]);
            this.elapseLabel.Alignment = ToolStripItemAlignment.Right;
            this.statusStrip1.Items.Add(this.elapseLabel);

            this.statusStrip1.ShowItemToolTips = true;
        }

        void SetupListView()
        {
            this.listView.ShowItemToolTips = true;
            this.listView.Dock = DockStyle.Fill;
            this.listView.FullRowSelect = true;
            this.listView.MultiSelect = false;
            this.listView.GridLines = true;
            this.listView.View = View.Details;
            this.listView.HeaderStyle = ColumnHeaderStyle.None;
            this.listView.VirtualMode = true;
            this.listView.VirtualListSize = 0;
            this.listView.SmallImageList = this.imageList1;

            int logColumnWidth = this.defaultFormSize.Width - this.timeColumnWidth - this.fromColumnWidth - 20;
            this.listView.Columns.Add("TimeStamp", this.timeColumnWidth);
            this.listView.Columns.Add("Log", logColumnWidth).Name = "Log";
            this.listView.Columns.Add("From", this.fromColumnWidth, HorizontalAlignment.Right).Name = "From";

            this.listView.Resize += (s, ev) =>
            {
                this.listView.Columns["Log"].Width = this.listView.ClientSize.Width - this.timeColumnWidth - this.listView.Columns["From"].Width - 20;
                if (this.listView.VirtualListSize > 0)
                {
                    this.listView.EnsureVisible(this.listView.VirtualListSize - 1);
                }
            };

            this.listView.ItemSelectionChanged += (s, ev) =>
            {
                this.SetScrollLock(true);

                if (ev.IsSelected)
                {
                    int index = (int)ev.Item.Tag;
                    if (0 <= index && index < this.logRecords.Count)
                    {
                        this.SetStatusText(this.logRecords[index]);
                    }
                }
                else
                {
                    this.SetStatusText();
                }
            };

            this.listView.CacheVirtualItems += (s, e) =>
            {
                if (itemCache.Count > this.maxItemCacheCount)
                {
                    itemCache.Clear();
                }
                for (int i = e.StartIndex; i <= e.EndIndex; i++)
                {
                    if (!this.itemCache.ContainsKey(i))
                    {
                        this.itemCache[i] = new ListViewItem(new string[3]);
                    }
                }
            };

            this.listView.RetrieveVirtualItem += (s, e) =>
            {
                if (!this.itemCache.TryGetValue(e.ItemIndex, out ListViewItem item))
                {
                    item = new ListViewItem(new string[3]);
                }

                if (0 <= e.ItemIndex && e.ItemIndex < this.filteredLogRecords.Count())
                {
                    var logRecord = this.filteredLogRecords.ElementAt(e.ItemIndex);
                    item.ImageIndex = this.GetImageIndexFromLevel(logRecord.Level);
                    item.SubItems[0].Text = logRecord.GetElapsedSecondsText();
                    item.SubItems[1].Text = logRecord.GetSingleLineLogText();
                    item.SubItems[2].Text = logRecord.CallerMethodName + " - " + logRecord.GetCallerLocationText();
                    item.Tag = logRecord.Index;
                }
                else
                {
                    item.ImageIndex = -1;
                    foreach (ListViewItem.ListViewSubItem subItem in item.SubItems)
                    {
                        subItem.Text = string.Empty;
                    }
                }

                e.Item = item;
            };

            this.listView.KeyDown += (s, e) =>
            {
                if (e.KeyCode == Keys.Escape)
                {
                    this.SetScrollLock(false);
                }
            };

            this.listView.MouseWheel += (s, e) => this.SetScrollLock(true);

            //Delay.SetInterval(() =>
            //{
            //    if (this.Visible)
            //    {
            //        //this.SuspendLayout();
            //        this.listView.Columns[2].AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent);
            //        this.listView.Columns["Log"].Width = this.listView.ClientSize.Width - this.timeColumnWidth - this.listView.Columns["From"].Width - 20;
            //        //this.ResumeLayout();
            //    }
            //}, 1000);
        }

        int GetImageIndexFromLevel(LogLevel level)
        {
            return this.imageList1.Images.IndexOfKey(this.logLevelToImageKey[level]);
        }

        /// <summary>
        /// ログ詳細テキストボックスを設定します。
        /// </summary>
        void SetStatusText(LogRecord? logRecord = null)
        {
            this.callerLabel.Text = string.Format("{0} - {1}",
                logRecord.Value.CallerMethodFullName,
                logRecord.Value.GetCallerLocationText());
        }

        /// <summary>
        /// 現在のフィルタ設定に基づいてログリストの内容を更新します。
        /// </summary>
        void UpdateListContent()
        {
            this.filteredLogRecords = this.GetFilteredLogRecords().ToArray();

            try
            {
                int count = this.filteredLogRecords.Count();
                if (!this.scrollLocked)
                {
                    this.listView.SelectedIndices.Clear();
                    if (this.listView.VirtualListSize > 0)
                    {
                        // 表示位置がわちゃわちゃしないためのまじない
                        this.listView.EnsureVisible(0);
                    }
                }

                this.listView.VirtualListSize = Math.Min(count, this.maxListViewLine);

                if (this.listView.VirtualListSize > 0)
                {
                    if (!this.scrollLocked)
                    {
                        this.listView.EnsureVisible(this.listView.VirtualListSize - 1);
                        this.SetStatusText(this.filteredLogRecords.Last());
                    }
                }
                else
                {
                    this.SetStatusText();
                }
            }
            catch (Exception ex)
            {
               Console.WriteLine(ex);
            }
        }

        IEnumerable<LogRecord> GetFilteredLogRecords()
        {
            // フィルタボタン反映
            var enables = new Dictionary<LogLevel, bool>();
            foreach (var level in this.filterButtons.Keys)
            {
                enables[level] = this.filterButtons[level].Checked;
            }
            var logRecords = this.logRecords.Where(info => enables[info.Level]);

            // フィルタテキストあれば反映
            var filterText = this.filterTextBox.Text;
            if (!string.IsNullOrEmpty(filterText))
            {
                filterText = filterText.ToLower();
                logRecords = logRecords.Where(info =>
                    (info.GetSingleLineLogText().ToLower().Contains(filterText) ||
                    info.GetCallerLocationText().ToLower().Contains(filterText)));
            }

            return logRecords;
        }

        /// <summary>
        /// ログレベルフィルタボタンのラベルを更新します。
        /// </summary>
        void UpdateFilterButtonLabel()
        {
            foreach (var level in this.filterButtons.Keys)
            {
                this.filterButtons[level].Text = string.Format("{0} ({1})", level.ToString(), this.countByLogLevel[level]);
            }
        }

        /// <summary>
        /// スクロール位置固定の有無を設定します。
        /// </summary>
        void SetScrollLock(bool locked = true)
        {
            if(this.scrollLocked && !locked)
            {
                this.listView.SelectedIndices.Clear();

                if (this.listView.VirtualListSize > 0)
                {
                    this.listView.EnsureVisible(this.listView.VirtualListSize - 1);
                    this.SetStatusText(this.filteredLogRecords.Last());
                }
                else
                {
                    this.SetStatusText();
                }
            }

            this.scrollLocked = locked;

            if (this.scrollLocked)
            {
                this.lockButton.Image = this.imageList1.Images["lock.png"];
                this.lockButton.Text = "ScrollLocked";
            }
            else
            {
                this.lockButton.Image = this.imageList1.Images["arrow_down.png"];
                this.lockButton.Text = "AutoScroll";
            }
        }

        /// <summary>
        /// ログをファイルに保存します。
        /// </summary>
        void SaveLogInfos(string directoryPath)
        {
            var name = string.Format("{0}_{1}.log",
                Util.GetApplicationName(),
                DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss"));
            var saveDialog = new SaveFileDialog() { FileName = name, Filter = "Log file|*.log", InitialDirectory = directoryPath };
            if (saveDialog.ShowDialog() == DialogResult.OK)
            {
                using (var writer = new StreamWriter(saveDialog.FileName))
                {
                    foreach (var logRecord in this.logRecords)
                    {
                        var sb = new StringBuilder();
                        if (string.IsNullOrEmpty(logRecord.GetCallerLocationText()))
                        {
                            sb.AppendFormat("{0,8}\t{1}\t{2}",
                                logRecord.GetElapsedSecondsText(),
                                logRecord.Level.ToString(),
                                logRecord.GetSingleLineLogText());
                        }
                        else
                        {
                            sb.AppendFormat("{0,8}\t{1}\t{2}\t({3} - {4})",
                                logRecord.GetElapsedSecondsText(),
                                logRecord.Level.ToString(),
                                logRecord.GetSingleLineLogText(),
                                logRecord.CallerMethodFullName,
                                logRecord.GetCallerLocationText());
                        }
                        sb.AppendLine();
                        writer.Write(sb.ToString());
                    }
                }
                this.saveButton.Enabled = false;
            }
        }
    }

    /// <summary>
    /// ログ一件分のデータ
    /// </summary>
    struct LogRecord
    {
        public int Index { get; private set; }
        public LogLevel Level { get; private set; }
        public double ElapsedSeconds { get; private set; }
        public string Log { get; private set; }
        public string CallerMethodName { get; private set; }
        public string CallerMethodFullName { get; private set; }
        public string CallerFileName { get; private set; }
        public int CallerLineNo { get; private set; }

        public LogRecord(int index, double elapsedSeconds, LogLevel level, string log, StackFrame frame)
        {
            this.Index = index;
            this.ElapsedSeconds = elapsedSeconds;
            this.Level = level;
            this.Log = log;
            this.CallerMethodName = frame.GetMethod().DeclaringType.Name ?? string.Empty;
            this.CallerMethodFullName = frame.GetMethod().DeclaringType.FullName ?? string.Empty;
            this.CallerFileName = Path.GetFileName(frame.GetFileName());
            this.CallerLineNo = frame.GetFileLineNumber();
        }

        public string GetElapsedSecondsText()
        {
            return string.Format("{0,8:0.000}", this.ElapsedSeconds);
        }

        public string GetSingleLineLogText()
        {
            return System.Text.RegularExpressions.Regex.Replace(this.Log, @"[\n\r\t]+", " ");
        }

        public string GetCallerLocationText()
        {
            return
                string.IsNullOrEmpty(this.CallerFileName) ?
                string.Empty :
                string.Format("{0}:{1}", this.CallerFileName, this.CallerLineNo.ToString());
        }
    }

    /// <summary>
    /// 非アクティブからのクリックに反応するToolStrip
    /// 参考：https://dobon.net/vb/bbs/log3-42/25280.html
    /// </summary>
    class ClickThruToolStrip : ToolStrip
    {
        protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);
            if (m.Msg == 0x21 && m.Result == (IntPtr)2)
            {
                m.Result = (IntPtr)1;
            }
        }
    }

    /// <summary>
    /// 非アクティブからのクリックに反応するStatusStrip
    /// 参考：https://dobon.net/vb/bbs/log3-42/25280.html
    /// </summary>
    class ClickThruStatusStrip : StatusStrip
    {
        protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);
            if (m.Msg == 0x21 && m.Result == (IntPtr)2)
            {
                m.Result = (IntPtr)1;
            }
        }
    }

    /// <summary>
    /// ちらつかないListView
    /// </summary>
    class FlickerFreeListView : ListView
    {
        public FlickerFreeListView()
        {
            this.DoubleBuffered = true;
        }
    }
}
