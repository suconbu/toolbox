﻿using System;
//using System.Windows.Forms;
using System.Timers;
using System.Collections.Concurrent;

namespace Suconbu.Toolbox
{
    /// <summary>
    /// 遅延実行のための静的メソッドを提供します。
    /// </summary>
    public class Delay
    {
        static Delay instance = new Delay();

        int nextTimerId = 0;
        ConcurrentDictionary<string, Timer> timeoutTimers = new ConcurrentDictionary<string, Timer>();
        ConcurrentDictionary<string, Timer> intervalTimers = new ConcurrentDictionary<string, Timer>();
        ConcurrentDictionary<Timer, TimerEntry> timerToEntry = new ConcurrentDictionary<Timer, TimerEntry>();

        /// <summary>
        /// 指定時間経過後に実行する処理を登録します。
        /// 登録された処理はメインスレッドで実行します。
        /// </summary>
        public static string SetTimeout(Action action, int delay, System.Windows.Forms.Control control, string timeoutKey = null, bool restartTimeout = false)
        {
            return instance.SetTimeoutInside(action, delay, control, timeoutKey, restartTimeout);
        }

        /// <summary>
        /// 指定時間経過後に実行する処理を登録します。
        /// 登録された処理は専用のスレッドで実行します。
        /// </summary>
        public static string SetTimeout(Action action, int delay, string timeoutKey = null, bool restartTimeout = false)
        {
            return instance.SetTimeoutInside(action, delay, null, timeoutKey, restartTimeout);
        }

        string SetTimeoutInside(Action action, int delay, System.Windows.Forms.Control control, string timeoutKey = null, bool restartTimeout = false)
        {
            ElapsedEventHandler handler = (s, e) =>
            {
                if (this.timerToEntry.TryRemove((Timer)s, out var entry))
                {
                    this.timeoutTimers.TryRemove(entry.Key, out var timer);
                    action();
                }
            };

            if (timeoutKey != null &&
                this.timeoutTimers.TryRemove(timeoutKey, out var existingTimer) &&
                this.timerToEntry.TryGetValue(existingTimer, out var existingEntry))
            {
                // 既存の登録を更新

                existingTimer.Elapsed -= existingEntry.Handler;
                existingTimer.Elapsed += handler;
                this.timeoutTimers[timeoutKey] = existingTimer;

                if (restartTimeout)
                {
                    existingTimer.Stop();
                    existingTimer.Start();
                }
            }
            else
            {
                // 新規登録

                if (timeoutKey == null)
                {
                    timeoutKey = this.GetNextTimerId().ToString();
                }

                var newTimer = new Timer(delay);
                newTimer.SynchronizingObject = control;
                newTimer.AutoReset = false;
                newTimer.Elapsed += handler;
                this.timeoutTimers[timeoutKey] = newTimer;
                this.timerToEntry[newTimer] = new TimerEntry(newTimer, timeoutKey, handler);

                newTimer.Start();
            }

            return timeoutKey;
        }

        /// <summary>
        /// SetTimeout で登録した処理を消去します。
        /// すでに実行後または消去済みの場合には何もしません。
        /// </summary>
        public static void ClearTimeout(string timeoutKey)
        {
            if (timeoutKey != null && instance.timeoutTimers.TryRemove(timeoutKey, out var timer))
            {
                instance.timerToEntry.TryRemove(timer, out var entry);
                timer.Stop();
            }
        }

        /// <summary>
        /// 周期的に実行する処理を登録します。
        /// 戻り値のインターバルIDは ClearTimeout による登録解除の際に必要です。
        /// </summary>
        public static string SetInterval(Action action, int delay)
        {
            var timer = new Timer(delay);
            timer.AutoReset = true;
            timer.Elapsed += (s, e) => action();
            timer.Start();

            string intervalKey = instance.GetNextTimerId().ToString();
            instance.intervalTimers[intervalKey] = timer;

            return intervalKey;
        }

        /// <summary>
        /// SetInterval で登録した処理を消去します。
        /// </summary>
        public static void ClearInterval(string intervalKey)
        {
            if (intervalKey != null && instance.intervalTimers.TryRemove(intervalKey, out var timer))
            {
                timer.Stop();
            }
        }

        int GetNextTimerId()
        {
            lock (this)
            {
                return this.nextTimerId++;
            }
        }

        Delay()
        {
        }

        class TimerTag
        {
            public int TimerId { get; set; }
            public string TimerKey { get; set; }
            public EventHandler Tick { get; set; }

            public TimerTag(int id, string key, EventHandler tick)
            {
                this.TimerId = id;
                this.TimerKey = key;
                this.Tick = tick;
            }
        }

        class TimerEntry
        {
            public Timer Timer { get; private set; }
            public string Key { get; private set; }
            public ElapsedEventHandler Handler { get; private set; }

            public TimerEntry(Timer timer, string key, ElapsedEventHandler handler)
            {
                this.Timer = timer;
                this.Key = key;
                this.Handler = handler;
            }
        }
    }
}
