﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Data;
using System.ComponentModel;
using System.Linq.Expressions;

namespace Suconbu.Toolbox
{
    /// <summary>
    /// DataGridView を拡張したコントロールです。
    /// - セルへのグラフ表示
    /// - 選択変化イベントの一時的な抑制
    /// </summary>
    class GridPanel : DataGridView
    {
        public ColorSet ColorSet { get; private set; }

        /// <summary>
        /// セルの塗りつぶし方法
        /// </summary>
        public enum CellPaintType { None, Fill, Bar }

        /// <summary>
        /// 通知を抑制できるイベント
        /// </summary>
		public enum SupressibleEvent
        {
            SelectedItemChanged
        }

        public interface ICellPainter
        {
            void Paint(GridPanel grid, DataGridViewCellPaintingEventArgs args);
        }

        /// <summary>
        /// セルを独自描画する時に参照するパラメータです。
        /// </summary>
        public class FillCellPainter : ICellPainter
        {
            public float Ratio { get; set; } = 0.0f;
            public Color Color { get; set; } = Color.Transparent;
            public void Paint(GridPanel grid, DataGridViewCellPaintingEventArgs args)
            {
                var ratio = Math.Min(Math.Max(0.0f, this.Ratio), 1.0f);
                int alpha = (int)(ratio * 180);
                var brush = grid.GetSolidBrush(Color.FromArgb(alpha, this.Color));
                args.Graphics.FillRectangle(brush, args.CellBounds);
            }
        }

        public class BarCellPainter : ICellPainter
        {
            public float Ratio { get; set; } = 0.0f;
            public Color Color { get; set; } = Color.Transparent;
            public void Paint(GridPanel grid, DataGridViewCellPaintingEventArgs args)
            {
                var fillRectangle = args.CellBounds;
                var ratio = Math.Min(Math.Max(0.0f, this.Ratio), 1.0f);
                fillRectangle.Width = (int)(fillRectangle.Width * ratio);
                fillRectangle.Inflate(0, -1);
                fillRectangle.Height--;
                var brush = grid.GetSolidBrush(this.Color);
                args.Graphics.FillRectangle(brush, fillRectangle);
            }
        }

        /// <summary>
        /// データを元に個々のセルを独自描画するための情報を持ちます。
        /// インスタンスを Tag メンバに設定して下さい。
        /// </summary>
        public interface ICellPainterProvider
        {
            ICellPainter GetCellPainter(GridPanel grid, DataGridViewColumn column, DataGridViewRow row);
        }

        public class NumericCellPaintData : ICellPainterProvider
        {
            public CellPaintType Type { get; set; } = CellPaintType.None;
            //public Func<GridPanel, DataGridViewColumn, DataGridViewRow, object, PaintParameter> Custom { get; set; } = null;
            //public object Parameter { get; set; } = null;
            public double MinValue { get; set; } = 0.0;
            public double MaxValue { get; set; } = 0.0;
            public Color PaintColor { get; set; } = Color.Transparent;

            public ICellPainter GetCellPainter(GridPanel grid, DataGridViewColumn column, DataGridViewRow row)
            {
                if (row.Cells[column.Index].ValueType != typeof(DBNull))
                {
                    var value = Convert.ToDouble(row.Cells[column.Index].Value);
                    value = Math.Max(this.MinValue, Math.Min(value, this.MaxValue));
                    float ratio = (float)((value - this.MinValue) / (this.MaxValue - this.MinValue));
                    if (this.Type == CellPaintType.Fill)
                    {
                        return new FillCellPainter() { Color = this.PaintColor, Ratio = ratio };
                    }
                    else if (this.Type == CellPaintType.Bar)
                    {
                        return new BarCellPainter() { Color = this.PaintColor, Ratio = ratio };
                    }
                }
                return null;
            }
        }

        /// <summary>
        /// キー列の指定、指定なしの場合は最初の列をキー列として採用
        /// </summary>
        public string KeyColumnName { get; set; }

        /// <summary>
        /// 選択が変化した場合に通知します。
        /// SuppressEvent にて通知の抑制ができます。
        /// </summary>
		public event EventHandler SuppressibleSelectionChanged = delegate { };

        /// <summary>
        /// ハイライト時の色を設定または取得します。
        /// </summary>
		public Color HighlightedCellBackColor { get; set; } = Color.Transparent;

        /// <summary>
        /// ハイライト表示する行のインデックスを設定または取得します。
        /// 設定後は自動的に再描画します。
        /// </summary>
        public int[] HighlightedRowIndices
        {
            get
            {
                return this.highlightedRowIndices;
            }
            set
            {
                if (this.highlightedRowIndices != null)
                {
                    // 今までハイライトだった行の再描画
                    Array.ForEach(this.highlightedRowIndices, i => { if (i < this.Rows.Count) this.InvalidateRow(i); });
                }
                if (value != null)
                {
                    // これからハイライトになる行の再描画
                    Array.ForEach(value, t => this.InvalidateRow(t));
                }
                this.highlightedRowIndices = value;
            }
        }

        /// <summary>
        /// 表示範囲の中央に位置させたい行のインデックスを設定します。
        /// </summary>
		public int CenterDisplayedRowIndex
        {
            set
            {
                int index = Math.Max(0, value - this.DisplayedRowCount(false) / 2);
                this.FirstDisplayedScrollingRowIndex = index;
            }
        }

        /// <summary>
        /// 未ソートの列ヘッダをクリックした時に昇順ソートから始める場合には true を設定します。
        /// </summary>
        public bool AccendFirst { get; set; } = false;

        bool selectionChangedReceived;
        int[] highlightedRowIndices;
        Dictionary<SupressibleEvent, int> suppressCounter = new Dictionary<SupressibleEvent, int>();
        Dictionary<Color, Brush> brushCache = new Dictionary<Color, Brush>();

        /// <summary>
        /// インスタンスを作成します。
        /// </summary>
		public GridPanel()
        {
            //this.view = view.Details;
            //this.GridLines = true;
            this.DoubleBuffered = true;
            this.RowHeadersVisible = false;
            this.AllowUserToAddRows = false;
            this.AllowUserToDeleteRows = false;
            this.AllowUserToOrderColumns = false;
            this.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            //this.AllowUserToResizeColumns = false;
            this.AllowUserToResizeRows = false;
            this.ReadOnly = true;
            this.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            this.ColumnAdded += GridView_ColumnAdded;
            this.CellPainting += GridView_CellPainting;
            this.ColumnHeaderMouseClick += GridView_ColumnHeaderMouseClick;
            this.SelectionChanged += GridView_SelectionChanged;
            this.RowContextMenuStripNeeded += GridView_RowContextMenuStripNeeded;

            foreach (SupressibleEvent name in Enum.GetValues(typeof(SupressibleEvent)))
            {
                this.suppressCounter[name] = 0;
            }

            //this.SelectedItemChangedIsEnabled = true;
            this.HighlightedRowIndices = null;
        }

        #region Public methods

        /// <summary>
        /// 表示状態を取得します。
        /// 表示状態にはソート列、選択範囲、スクロール位置が含まれます。
        /// </summary>
        public GridViewState GetViewState()
        {
            return new GridViewState(this, this.KeyColumnName);
        }

        /// <summary>
        /// GetViewState で取得しておいた表示状態を設定します。
        /// </summary>
		public void SetViewState(GridViewState viewState, GridViewState.ApplyTargets flags = GridViewState.ApplyTargets.All)
        {
            viewState.ApplyTo(this, flags);
        }

        /// <summary>
        /// 全ての列の幅を設定します。
        /// </summary>
        public void SetAllColumnWidth(int width)
        {
            foreach (DataGridViewColumn column in this.Columns)
            {
                column.Width = width;
            }
        }

        /// <summary>
        /// 通知を抑制します。
        /// </summary>
        public void SuppressEvent(SupressibleEvent @event)
        {
            this.suppressCounter[@event]++;
        }

        /// <summary>
        /// 通知の抑制を解除します。
        /// </summary>
        public void UnsuppressEvent(SupressibleEvent @event)
        {
            if (this.suppressCounter[@event] <= 0)
            {
                throw new InvalidOperationException();
            }

            this.suppressCounter[@event]--;

            if (this.suppressCounter[@event] == 0 && this.selectionChangedReceived)
            {
                this.SuppressibleSelectionChanged(this, new EventArgs());
            }
        }

        /// <summary>
        /// 指定された文字列を名前として列を追加し、その列を返します。
        /// </summary>
        public DataGridViewColumn AddColumn(string columnName, string headerText = null)
        {
            int index = this.Columns.Add(columnName, headerText);
            return this.Columns[index];
        }

        /// <summary>
        /// 新しい行を追加し、その行を返します。
        /// </summary>
        public DataGridViewRow AddRow(params object[] values)
        {
            int index = this.Rows.Add(values);
            return this.Rows[index];
        }

        /// <summary>
        /// 指定された列を並べ替えます。
        /// 「降順→昇順→キー列の昇順」の順序でサイクルします。
        /// </summary>
        public void SortColumn(DataGridViewColumn column, ListSortDirection direction)
        {
            this.SortColumnInside(column, direction);
        }

        /// <summary>
        /// 色設定を反映します。
        /// </summary>
        public void ApplyColorSet(ColorSet colorSet)
        {
            this.BackgroundColor = colorSet.Back;
            this.ForeColor = colorSet.Text;
            this.GridColor = colorSet.GridLine;
            this.DefaultCellStyle.BackColor = colorSet.Back;
            this.DefaultCellStyle.ForeColor = colorSet.Text;
            this.DefaultCellStyle.SelectionBackColor = colorSet.SelectionBack;
            this.DefaultCellStyle.SelectionForeColor = colorSet.SelectionText;

            this.ColorSet = colorSet;
        }

        #endregion Public methods

        #region Private methods

        void GridView_RowContextMenuStripNeeded(object sender, DataGridViewRowContextMenuStripNeededEventArgs e)
        {
            // 複数選択状態での右クリック時は今の選択状態を維持したい
            if (this.SelectedRows.Count <= 1)
            {
                if (e.RowIndex >= 0)
                {
                    this.ClearSelection();
                    this.Rows[e.RowIndex].Selected = true;
                }
            }
        }

        void GridView_SelectionChanged(object sender, EventArgs e)
        {
            if (this.suppressCounter[SupressibleEvent.SelectedItemChanged] == 0)
            {
                this.SuppressibleSelectionChanged(this, e);
            }
            else
            {
                this.selectionChangedReceived = true;
            }
        }

        void GridView_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            // 降順→昇順になるように
            var column = this.Columns[e.ColumnIndex];
            if (column.SortMode == DataGridViewColumnSortMode.NotSortable)
            {
                return;
            }

            var firstDirection = this.AccendFirst ? ListSortDirection.Ascending : ListSortDirection.Descending;
            ListSortDirection nextDirection;
            if (this.SortedColumn != null && this.SortedColumn.Index == e.ColumnIndex)
            {
                nextDirection =
                    this.SortOrder == SortOrder.Ascending ?
                    ListSortDirection.Descending :
                    ListSortDirection.Ascending;

                if (this.KeyColumnName != null && nextDirection == firstDirection)
                {
                    column = this.Columns[this.KeyColumnName];
                    nextDirection = ListSortDirection.Ascending;
                }
            }
            else
            {
                nextDirection = firstDirection;
            }

            this.SortColumnInside(column, nextDirection);
        }

        void GridView_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            e.Handled = false;

            if (e.RowIndex < 0)
            {
                return;
            }

            var backParts = e.PaintParts & (DataGridViewPaintParts.Background | DataGridViewPaintParts.SelectionBackground);
            var nonBackParts = e.PaintParts & ~backParts;

            e.Paint(e.ClipBounds, backParts);

            if (this.HighlightedRowIndices != null && Array.IndexOf(this.HighlightedRowIndices, e.RowIndex) >= 0)
            {
                e.Graphics.FillRectangle(new SolidBrush(this.HighlightedCellBackColor), e.CellBounds);
            }

            var column = this.Columns[e.ColumnIndex];
            var row = this.Rows[e.RowIndex];

            var paintInfo = column.Tag as ICellPainterProvider;
            if (paintInfo == null)
            {
                paintInfo = row.Tag as ICellPainterProvider;
                if (paintInfo == null)
                {
                    paintInfo = row.Cells[e.ColumnIndex].Tag as ICellPainterProvider;
                }
            }

            if (paintInfo != null)
            {
                var cellPainter = paintInfo.GetCellPainter(this, column, row);
                if (cellPainter != null)
                {
                    cellPainter.Paint(this, e);
                }
            }

            e.Paint(e.ClipBounds, nonBackParts);

            e.Handled = true;
        }

        void GridView_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            e.Column.SortMode = DataGridViewColumnSortMode.Programmatic;
        }

        void SortColumnInside(DataGridViewColumn column, ListSortDirection direction)
        {
            // ソートすると選択状態が変化するので元通り復帰してあげる
            this.SuppressEvent(SupressibleEvent.SelectedItemChanged);
            var viewState = this.GetViewState();

            this.Sort(column, direction);

            this.SetViewState(viewState, GridViewState.ApplyTargets.All & ~GridViewState.ApplyTargets.SortedColumn);
            this.UnsuppressEvent(SupressibleEvent.SelectedItemChanged);
        }

        Brush GetSolidBrush(Color color)
        {
            if (!this.brushCache.TryGetValue(color, out var brush))
            {
                brush = new SolidBrush(color);
                this.brushCache[color] = brush;
            }
            return brush;
        }

        #endregion Private methods
    }

    /// <summary>
    /// 表示状態を保持するクラス
    /// </summary>
    class GridViewState
    {
        [Flags]
        public enum ApplyTargets
        {
            SortedColumn = 0x1,
            Selection = 0x2,
            ScrollPosition = 0x4,
            All = SortedColumn | Selection | ScrollPosition
        }

        /// <summary>
        /// 選択行を指すキーのリストを取得します。
        /// </summary>
        public List<object> KeyOfSelectedRows { get; }

        /// <summary>
        /// リストの一番上に表示されている行のインデックス
        /// </summary>
        public int FirstDisplayedRowIndex { get; set; }

        internal string KeyColumnName { get; }

        string sortedColumnName;
        ListSortDirection sortDirection;

        /// <summary>
        /// インスタンスを作成します。
        /// keyColumnName が null の場合には選択状態は保持されません。
        /// </summary>
        internal GridViewState(GridPanel targetView, string keyColumnName)
        {
            // ソート列
            this.sortedColumnName = (targetView.SortedColumn != null) ? targetView.SortedColumn.Name : null;
            this.sortDirection = (targetView.SortOrder == SortOrder.Ascending) ? ListSortDirection.Ascending : ListSortDirection.Descending;

            // 選択状態
            this.KeyOfSelectedRows = new List<object>();
            if (keyColumnName != null)
            {
                this.KeyColumnName = keyColumnName;
                foreach (DataGridViewRow row in targetView.Rows)
                {
                    if (row.Selected)
                    {
                        this.KeyOfSelectedRows.Add(row.Cells[keyColumnName].Value);
                    }
                }
            }

            // スクロール位置
            this.FirstDisplayedRowIndex = targetView.FirstDisplayedScrollingRowIndex;
        }

        /// <summary>
        /// 指定された表示状態を適用します。
        /// </summary>
        internal void ApplyTo(GridPanel targetView, ApplyTargets flags)
        {
            // ソート列
            if ((flags & ApplyTargets.SortedColumn) != 0 && this.sortedColumnName != null)
            {
                targetView.Sort(targetView.Columns[this.sortedColumnName], this.sortDirection);
            }

            // 選択状態
            if ((flags & ApplyTargets.Selection) != 0)
            {
                targetView.SuppressEvent(GridPanel.SupressibleEvent.SelectedItemChanged);
                targetView.ClearSelection();
                foreach (var key in this.KeyOfSelectedRows)
                {
                    foreach (DataGridViewRow row in targetView.Rows)
                    {
                        if (row.Cells[this.KeyColumnName].Value.ToString() == key.ToString())
                        {
                            row.Selected = true;
                            break;
                        }
                    }
                }
                targetView.UnsuppressEvent(GridPanel.SupressibleEvent.SelectedItemChanged);
            }

            // スクロール位置
            if ((flags & ApplyTargets.ScrollPosition) != 0 && targetView.FirstDisplayedScrollingRowIndex >= 0 && this.FirstDisplayedRowIndex >= 0)
            {
                targetView.FirstDisplayedScrollingRowIndex = (this.FirstDisplayedRowIndex < targetView.RowCount) ? this.FirstDisplayedRowIndex : (targetView.RowCount - 1);
            }
        }
    }
}
