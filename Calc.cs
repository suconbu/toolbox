﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;

namespace Suconbu.Toolbox
{
    public static class Calc
    {
        /// <summary>
        /// 2点間の距離を算出します。
        /// </summary>
        public static double GetDistance(this Point from, Point to)
        {
            return Math.Sqrt(Math.Pow(to.X - from.X, 2.0) + Math.Pow(to.Y - from.Y, 2.0));
        }

        public static double GetDistance(this PointF from, PointF to)
        {
            return Math.Sqrt(Math.Pow(to.X - from.X, 2.0) + Math.Pow(to.Y - from.Y, 2.0));
        }

        /// <summary>
        /// 中央値を算出します。
        /// </summary>
        public static double Median(this IEnumerable<double> values)
        {
            var sorted = values.OrderBy(v => v);
            int count = values.Count();
            var a = sorted.ElementAt((count - 1) / 2);
            var b = sorted.ElementAt(count / 2);
            return (a + b) / 2.0;
        }

        /// <summary>
        /// 中央値を算出します。
        /// </summary>
        public static double Median(this IEnumerable<float> values)
        {
            var sorted = values.OrderBy(v => v);
            int count = values.Count();
            var a = sorted.ElementAt((count - 1) / 2);
            var b = sorted.ElementAt(count / 2);
            return (a + b) / 2.0;
        }

        /// <summary>
        /// 中央値を算出します。
        /// </summary>
        public static double Median(this IEnumerable<int> values)
        {
            var sorted = values.OrderBy(v => v);
            int count = values.Count();
            var a = sorted.ElementAt((count - 1) / 2);
            var b = sorted.ElementAt(count / 2);
            return (a + b) / 2.0;
        }
    }
}
