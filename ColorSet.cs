﻿using System.Drawing;
using System.IO;
using System.Text.RegularExpressions;
using System.Globalization;
using System;

namespace Suconbu.Toolbox
{
    class ColorSet
    {
        public string Name { get; private set; }
        public Color Back { get; private set; }
        public Color Text { get; private set; }
        public Color GrayedText { get; private set; }
        public Color SelectionBack { get; private set; }
        public Color SelectionText { get; private set; }
        public Color GridLine { get; private set; }
        public Color Accent1 { get; private set; }
        public Color Accent2 { get; private set; }
        public Color Accent3 { get; private set; }

        public static ColorSet Light
        {
            get
            {
                return new ColorSet()
                {
                    Name = "Light",
                    Back = Color.FromArgb(245, 245, 245),
                    Text = Color.FromArgb(30, 30, 30),
                    GrayedText = Color.FromArgb(160, 160, 162),
                    SelectionBack = Color.FromArgb(222, 223, 231),
                    SelectionText = Color.FromArgb(30, 30, 30),
                    GridLine = Color.FromArgb(204, 206, 219),
                    Accent1 = Color.FromArgb(122, 193, 255),
                    Accent2 = Color.FromArgb(255, 204, 0),
                    Accent3 = Color.FromArgb(229, 20, 0),
                };
            }
        }

        public static ColorSet Dark
        {
            get
            {
                return new ColorSet()
                {
                    Name = "Dark",
                    Back = Color.FromArgb(37, 37, 38),
                    Text = Color.FromArgb(241, 241, 241),
                    SelectionBack = Color.FromArgb(51, 51, 55),
                    SelectionText = Color.FromArgb(241, 241, 241),
                    GridLine = Color.FromArgb(63, 63, 70),
                    GrayedText = Color.FromArgb(95, 95, 97),
                    Accent1 = Color.FromArgb(60, 80, 80),
                    Accent2 = Color.FromArgb(197, 200, 22),
                    Accent3 = Color.FromArgb(216, 80, 80),
                };
            }
        }

        public static ColorSet FromVsScheme(string path)
        {
            var nameRe = new Regex("Name=\"([^\"]+)\"", RegexOptions.IgnoreCase);
            var foreRe = new Regex("Foreground=\"([^\"]+)\"", RegexOptions.IgnoreCase);
            var backRe = new Regex("Background=\"([^\"]+)\"", RegexOptions.IgnoreCase);

            var colorSet = new ColorSet();
            try
            {
                using (var reader = new StreamReader(path))
                {
                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine().TrimStart();

                        if (line.StartsWith("<Item "))
                        {
                            var name = nameRe.Match(line)?.Groups[1].Value;
                            var fore = Color.FromArgb(255, Color.FromArgb(Convert.ToInt32(foreRe.Match(line)?.Groups[1].Value, 16)));
                            var back = Color.FromArgb(255, Color.FromArgb(Convert.ToInt32(backRe.Match(line)?.Groups[1].Value, 16)));
                            if (name == "Plain Text")
                            {
                                colorSet.Text = fore;
                                colorSet.Back = back;
                            }
                            else if (name == "Comment")
                            {
                                colorSet.GrayedText = fore;
                            }
                            else if (name == "Selected Text")
                            {
                                colorSet.SelectionText = fore;
                                colorSet.SelectionBack = back;
                            }
                        }
                    }
                }
                colorSet.GridLine = Color.FromArgb(
                    Math.Min(colorSet.Text.R, colorSet.Back.R) + (colorSet.Text.R - colorSet.Back.R) * 10 / 100,
                    Math.Min(colorSet.Text.G, colorSet.Back.G) + (colorSet.Text.G - colorSet.Back.G) * 10 / 100,
                    Math.Min(colorSet.Text.B, colorSet.Back.B) + (colorSet.Text.B - colorSet.Back.B) * 10 / 100);
                var baseColorSet = colorSet.Back.GetBrightness() < 0.5 ? Dark : Light;
                colorSet.Accent1 = baseColorSet.Accent1;
                colorSet.Accent2 = baseColorSet.Accent2;
                colorSet.Accent3 = baseColorSet.Accent3;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                colorSet = null;
            }

            return colorSet;
        }

        ColorSet()
        {
        }
    }
}
